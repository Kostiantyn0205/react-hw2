import PropTypes from 'prop-types';
import IconStar from "../icon/IconStar";
import IconBasket from "../icon/IconBasket";

const Header = ({counterFavorites, counterSelected}) => {
    return <header>
        <h1 className={"title"}>Sports Car Store</h1>
        <div className="container-icon">
            <div className="icon-wrapper">
                <IconStar />
                <div className="counter counter-favorites">{counterFavorites}</div>
            </div>
            <div className="icon-wrapper">
                <IconBasket />
                <div className="counter counter-selected">{counterSelected}</div>
            </div>
        </div>
    </header>
}

Header.propTypes = {
    counterFavorites: PropTypes.number,
    counterSelected: PropTypes.number
};

export default Header