import Button from "./Button";
import PropTypes from 'prop-types';
import IconStarCard from "../icon/IconStarCard";

function ProductCard({id, racingCar, onClick, toggleCounterFavorites}) {
    const {imageUrl, name, color, price, completed} = racingCar;
    return (
        <li className="car-item">
            <div className="img-container">
                <img className={"car-img"} src={imageUrl} alt={name}/>
            </div>
            <h2 className="car-name">{name}</h2>
            <p className="car-info">Цвет: {color}</p>
            <p className="car-info">Цена: ${price.toLocaleString()}</p>
            <Button text="Add to cart" onClick={onClick}/>
            <div onClick={() => {toggleCounterFavorites(id)}} className={`car-item-icon ${completed && "completed"}`}>
                <IconStarCard />
            </div>
        </li>
    );
}

ProductCard.propTypes = {
    id: PropTypes.number,
    racingCar: PropTypes.object,
    onClick: PropTypes.func,
    toggleCounterFavorites: PropTypes.func
};

export default ProductCard;