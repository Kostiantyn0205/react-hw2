import ProductCard from "./ProductCard";
import PropTypes from 'prop-types';

function MainPage({racingCars, onClick, toggleCounterFavorites}) {
    return (
        <ul className="car-list">
            {racingCars.map((car) => (
                <ProductCard key={car.idCar} id={car.idCar} racingCar={car} onClick={onClick} toggleCounterFavorites={toggleCounterFavorites}/>
            ))}
        </ul>
    );
}

MainPage.propTypes = {
    racingCars: PropTypes.array,
    onClick: PropTypes.func,
    toggleCounterFavorites: PropTypes.func
};

export default MainPage;