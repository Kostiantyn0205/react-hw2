import './scss/App.scss';
import Button from "./components/Button";
import Modal from "./components/Modal";
import {useEffect, useState} from 'react';
import ListCard from "./components/ListCard;";
import Header from "./components/Header";

function App() {
    const localStorageDataFavorites = JSON.parse(localStorage.getItem("counterFavorites"));
    const localStorageDataSelected = JSON.parse(localStorage.getItem("counterSelected"));
    const localStorageRacingCars = JSON.parse(localStorage.getItem("racingCars"));

    const [showModal, setShowModal] = useState(false);
    const [counterFavorites, setCounterFavorites] = useState( localStorageDataFavorites || 0);
    const [counterSelected, setCounterSelected] = useState( localStorageDataSelected || 0);
    const [racingCars, setRacingCars] = useState(localStorageRacingCars || []);

    // localStorage.clear();
    useEffect(() => {
        localStorage.setItem("counterFavorites", JSON.stringify(counterFavorites));
        localStorage.setItem("counterSelected", JSON.stringify(counterSelected));
    }, [counterSelected, counterFavorites]);

    useEffect(() => {
        if (!localStorageRacingCars) {
            fetch('/products.json')
                .then((response) => {
                    if (!response.ok) {
                        throw new Error('Ошибка при выполнении запроса');
                    }
                    return response.json();
                })
                .then((data) => {
                    data.racingCars.forEach((data, index) => {
                        data.completed = false;
                        data.idCar = index;
                        data.buyCar = false;
                        index++;
                    });
                    setRacingCars(data.racingCars);
                })
                .catch((error) => {
                    console.error('Ошибка при выполнении запроса:', error);
                });
        }
    }, []);

    const toggleModal = () => {
        setShowModal(!showModal);
    };

    const toggleCounterFavorites = (id) => {
        const newRacingCars = [...racingCars];
        newRacingCars[id].completed = !newRacingCars[id].completed;
        newRacingCars[id].completed? setCounterFavorites(counterFavorites+1):setCounterFavorites(counterFavorites-1);
        setRacingCars(newRacingCars);
        localStorage.setItem("racingCars", JSON.stringify(newRacingCars));
    }

    const toggleCounterSelected = () => {
        setCounterSelected(counterSelected + 1);
    }

    const modalActions = (
        <div className="button-container">
            <Button backgroundColor="rgba(0, 0, 0, 0.4)" textColor="white" text="Yes" onClick={() => {toggleModal(); toggleCounterSelected()}}/>
            <Button backgroundColor="rgba(0, 0, 0, 0.4)" textColor="white" text="No" onClick={toggleModal}/>
        </div>
    );

    return (
        <div className="App">
            <Header counterFavorites={counterFavorites} counterSelected={counterSelected}/>
            <ListCard racingCars={racingCars} onClick={toggleModal} toggleCounterFavorites={toggleCounterFavorites}></ListCard>
            {showModal && (<Modal header="Confirmation" closeButton={true} onClick={toggleModal} text="Are you sure you want to add the product to the cart?" actions={modalActions}/>)}
        </div>
    );
}

export default App;